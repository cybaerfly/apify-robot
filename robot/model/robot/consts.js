module.exports = {
    tasks: {
        login: 'login',
        action: 'action',
    },
    steps: {
        decryptSecrets: 'decryptSecrets',
        queryProxyIp: 'queryProxyIp',
        prepareLogin: 'prepareLogin',
        attemptLogin: 'attemptLogin',
        prepareAccount: 'prepareAccount',
        prepareAction: 'prepareAction',
        promptAction: 'promptAction',
        finishAction: 'finishAction',
        verifyAction: 'verifyAction',
        handleErrors: 'handleErrors',
    },
};
